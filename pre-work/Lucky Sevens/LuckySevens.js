var money;
var count;
var currentBalance;
var highestBalance;
var rollOne;
var rollTwo;
var totalRoll;

//prompt
function bet(){
	document.getElementById("playGame").style.display = "";
	document.getElementById("myTable").style.visibility = "hidden";
	document.getElementById("newGame").style.display = "none";
	
	
	do {
		money = Number(prompt("How many dollars do you have to bet on today?"));
		if(money < 1 || isNaN(money) === true){	
				alert("THAT IS AN INVALID ENTRY"); 
		}
	}
	while(money < 1 || isNaN(money) === true); 
	
	if (money >= 1){
		document.getElementById("startingBet2").innerHTML = "$" + money + ".00";			
		document.getElementById("highestAmount").innerHTML = "$" + money + ".00";
		document.getElementById("startingBet").value = money;
	}
}
		
//Play
function play(){
		count = 0;
		currentBalance = money;
		highestBalance = money;				
		while(currentBalance >= 1){
			rollOne = Math.floor((Math.random() * 6) + 1);	 	
			rollTwo = Math.floor((Math.random() * 6) + 1);
			totalRoll = rollOne + rollTwo;
			if (totalRoll === 7){
				currentBalance = currentBalance + 4;
			} else {
				currentBalance = currentBalance - 1;						
			}
//Total Rolls		
			count = ++count;						
			document.getElementById("totalRolls").innerHTML = count;
//Highest Amount Won		
			if (currentBalance > highestBalance) {
				highestBalance = currentBalance;
				document.getElementById("highestAmount").innerHTML = "$" + highestBalance + ".00";
//Rollcount at Highest Amount Amount Won			
				document.getElementById("rollCountAtHighest").innerHTML = count;
			}	
			}		
		if (currentBalance === 0) {
		document.getElementById("playGame").style.display = "none";		
		document.getElementById("newGame").style.display = "";	
		document.getElementById("myTable").style.visibility = "visible";
		}	
}



	
